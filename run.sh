#! /bin/bash

# welcome message
cat << EOF
+----------------------------------------------------------------+
|                                                                |
|                                                                |
|          Differential Expression Analysis Workflow             |
|                  By Sam Chen & Nikhil Gowda                    |
|                                                                |
|                                                                |
+----------------------------------------------------------------+

EOF

# checks if the file was sourced
if [[ ${BASH_SOURCE[0]} != $0 ]]; then

cat << EOF
This script is being sourced, so it may not run properly.
Please quit and run this workflow within a subshell.
Use the command: 'bash path/to/run.sh'
EOF

  read -n 1 -s -r -p "Press CTRL+C to quit or press any key to continue sourcing."
  echo
fi

# Find directory program was started from
ORI=$(pwd)
# Find program folder
DIR="$( readlink -e "$( dirname "${BASH_SOURCE[0]}" )" )"
# Sourcing helpers and Functions
. "$DIR"/helper.sh
. "$DIR"/functions.sh

# Ask whether we wanna use default configs or set anew
# P_DIR, starGenome, REF_GENOME, GTF, GA, RESET
if [[ -f $DIR/.config ]]; then
  if [ $# == 2 ]; then
    echo "Using saved configuration."
    . "$DIR"/.config
  else
    while true; do
        read -p "Use saved configuration? " yn
        case $yn in
            [Yy1]* ) . "$DIR"/.config; break;;
            [Nn0]* ) . "$DIR"/init.sh; break;;
            * ) echo "Please answer yes or no. ";;
        esac
    done
  fi
else
  . "$DIR"/init.sh
fi

# get group A and group B files by folder or by positional parameters

if [ $# == 2 ]; then
  if [[ ! -d "$1" ]] || [[ ! -d "$2" ]]; then
    echo "Error: Invalid directory!"
    exit
  fi
  GROUP_A_DIR="$(readlink -e "$1")"
  GROUP_A_NAME="$(basename "$GROUP_A_DIR")"
  GROUP_B_DIR="$(readlink -e "$2")"
  GROUP_B_NAME="$(basename "$GROUP_B_DIR")"
  IFS=$'\n'
  GROUP_A=($(readlink -e "$GROUP_A_DIR"/* | grep .fastq$))
  GROUP_B=($(readlink -e "$GROUP_B_DIR"/* | grep .fastq$))
  unset IFS
else
  echo
  read -p "Please input the name of group A. " GROUP_A_NAME;
  read -p "Please input the name of group B. " GROUP_B_NAME;
  echo "Please make sure $GROUP_A_NAME and $GROUP_B_NAME fastq files are separated into different folders."
  echo "Please input group $GROUP_A_NAME directory path"
  getDir GROUP_A_DIR;
  IFS=$'\n'
  GROUP_A=($(readlink -e "$GROUP_A_DIR"/* | grep .fastq$))
  unset IFS
  echo "Found ${#GROUP_A[@]} fastq files."
  echo
  echo "Please input group $GROUP_B_NAME directory path"
  getDir GROUP_B_DIR;
  IFS=$'\n'
  GROUP_B=($(readlink -e "$GROUP_B_DIR"/* | grep .fastq$))
  unset IFS
  echo "Found ${#GROUP_B[@]} fastq files."
fi
GROUP_ALL=("${GROUP_A[@]}" "${GROUP_B[@]}")

# make results and log folder names particular to this run
# c=$(echo $b | tr -cd [a-z0-9\-\_])
LOGS="$( echo "$GROUP_A_NAME-$GROUP_B_NAME"_logs | tr -cd [A-Za-z0-9\-\_])"
RESULTS="$( echo "$GROUP_A_NAME-$GROUP_B_NAME"_results | tr -cd [A-Za-z0-9\-\_])"

# checks if logs and results folders already exist and if so asks whether or not to replace
dirExists "$P_DIR" "$LOGS"
dirExists "$P_DIR" "$RESULTS"


mkdir "$P_DIR"/"$LOGS"
cat << EOF
Workflow starting in background with PID: ${$}
Logs will be stored in $P_DIR/$LOGS
Results will be stored in $P_DIR/$RESULTS
If this script was started using the bash function
then it will continue running after log out
EOF

cat << EOF >> "$P_DIR"/"$LOGS"/log.out
Starting workflow on files:
  $GROUP_A_NAME (${#GROUP_A[@]}) : ${GROUP_A[@]}
  $GROUP_B_NAME (${#GROUP_B[@]}) : ${GROUP_B[@]}
Results stored in: $P_DIR/$RESULTS
Logs stored in: $P_DIR/$LOGS

----

EOF

. "$DIR"/workflow.sh >> "$LOGS"/log.out 2>&1 &

cd $ORI
